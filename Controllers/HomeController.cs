﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NumberToWords.Helpers;

namespace NumberToWords.Controllers
{
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        [HttpGet("ValidateNumber")]
        public JsonResult Get(string number)
        {
            decimal res = 0.00M;
            string result = string.Empty;
            bool status = false;

            if(decimal.TryParse(number, out res))
            {
                result = $"**{ Numbers.NumberToWords(res) } pesos only**";
                status = true;
            }

            return Json(new
            {
                result = result,
                status = status
            });

        }
    }
}
