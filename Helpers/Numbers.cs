using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NumberToWords.Helpers
{
    public class Numbers
    {
        public static string NumberToWords(decimal n)
        {
            string index0 = string.Empty,
                   index1 = string.Empty;
            long number = 0;
            if (n.ToString().Contains("."))
            {
                string index_0 = n.ToString().Split('.')[0],
                       index_1 = n.ToString().Split('.')[1];

                if (index_0.Length > 19)
                {
                    return "Out of bounds!";
                }

                number = long.Parse(index_0);
                long secondNumber = long.Parse(index_1);
                if (number <= 0)
                {
                    return $" { secondNumber }/100";
                }

                if (secondNumber > 0)
                {
                    string secondNum = (secondNumber < 10) ? string.Concat("0",secondNumber) : secondNumber.ToString();
                    index1 = $"{ secondNum }/100";
                }
            }
            else
            {
                number = (long)n;
            }

            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000000000000000) > 0)
            {
                words += NumberToWords(number / 1000000000000000000) + " quintillion ";
                number %= 1000000000000000000;
            }

            if ((number / 1000000000000000) > 0)
            {
                words += NumberToWords(number / 1000000000000000) + " quadrillion ";
                number %= 1000000000000000;
            }

            if ((number / 1000000000000) > 0)
            {
                words += NumberToWords(number / 1000000000000) + " trillion ";
                number %= 1000000000000;
            }

            if ((number / 1000000000) > 0)
            {
                words += NumberToWords(number / 1000000000) + " billion ";
                number %= 1000000000;
            }

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                /*
                if (words != "")
                    words += "";//and
                */
                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            string result = !string.IsNullOrEmpty(index1) ? string.Concat(words, " & ", index1) : words;
            return result;
        }
    }
}
